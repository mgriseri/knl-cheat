#!/usr/bin/env python3

"""
'Kingdom: New Lands' cheat script.

DESCRIPTION:
This is a python script to cheat in the game 'Kingdom: New Lands'

It works by searching & replacing entries in the game save file.
It should work on Win, Mac and Linux.

It has been tested using :
- Kingdom: New Lands v1.2.8 R2121 (from www.gog.com)
On:
- macOS: El Capitan 10.11.6
- Linux: openSUSE Leap 42.3

NOTES:
- If something goes wrong,
  The script creates a backup of your save file,
  the first time you run it.
  It is named 'storage_v34_AUTO.dat.backup'.

  Renaming it to 'storage_v34_AUTO.dat' should restore
  the save file.

LICENSE:
GNU GPL v3
https://www.gnu.org/licenses/gpl.html

Made with <3 by Michael Griseri.
"""

import argparse
import json
import os
import shutil
import subprocess
import sys

# Constants
USER_HOME = os.path.expanduser("~")
SCRIPT_NAME = sys.argv[0]
SCRIPT_CACHE_NAME = 'knl-cheat-cache.txt'
SAVE_FILE = 'storage_v34_AUTO.dat'  # is a json file
BACKUP_FILE = 'storage_v34_AUTO.dat.backup'


def find_savefile():
    """
    Looks for the save file.
    Returns the directory of the save file if found.

    Scans the whole $HOME directory for a game save file.
    when found, stores its path in a file (for next time) and return it.
    """
    savefile_found = False

    try:
        with open(SCRIPT_CACHE_NAME, 'r') as cached_savefile_dir:
            search_start = cached_savefile_dir.read()
    except FileNotFoundError:
        print('Locating your save file. Please wait...')
        search_start = USER_HOME  # scan entire home if no cache

    for root, dirs, files in os.walk(search_start):
        for name in files:
            if name == SAVE_FILE:
                savefile_path = os.path.join(root, name)
                savefile_dir = os.path.dirname(savefile_path)
                if 'kingdom' in savefile_path.lower():
                    savefile_found = True
                    break
                break

    if savefile_found:
        # write save file location to a file
        # and return it
        with open(SCRIPT_CACHE_NAME, 'w') as cached_savefile_dir:
            cached_savefile_dir.write(savefile_dir)
        return savefile_dir
    else:
        return False


# SAVE FILE I/O FUNCTIONS:
def load_save_file_for_py():
    """
    copies content of save file in a variable.
    loads raw (json) content as python format.
    define new global variables (I know...) so the script can
    read the file as a standard python dict.
    It *must* be used with the close_save_file_for_py() function.
    Otherwise, the changes aren't written to the actual file.
    """
    with open(SAVE_FILE) as savefile:
        savefile_as_json = savefile.read()

    # file variable:
    global savefile_as_py
    savefile_as_py = json.loads(savefile_as_json)

    # file variables:
    global obj_list
    global player_preview
    obj_list = savefile_as_py['objects']
    player_preview = savefile_as_py['playerPreview']


def close_save_file_for_py():
    """
    converts modified python data into json.
    write to new json to hard drive.
    """
    savefile_as_json_new = json.dumps(savefile_as_py)
    with open(SAVE_FILE, 'w') as savefile:
        savefile.write(savefile_as_json_new)


# CHEAT FUNCTIONS:
def money_hack():
    """
    Change wallet data in the Player object.
    Fill player's wallet.
    """
    load_save_file_for_py()

    for obj in obj_list:
        obj_path = obj['prefabPath']
        obj_data = obj['componentData2']
        obj_index = obj_list.index(obj)

        # in the Player object,
        # find the wallet data,
        # change str containing coins ammount.
        if 'Prefabs/Characters/Player' in obj_path:
            for data in obj_data:
                if data['type'] == 'WalletData':
                    (obj_list[obj_index]
                        ['componentData2']
                        [obj_data.index(data)]
                        ['data']) = '{"coins":38}'

                    print('Wallet is now full.')
                    break

            else:
                print('ERROR: Unable to change wallet data.')
    close_save_file_for_py()


def bank_money():
    """
    Look for the Banker object in savefile.
    Change its data to 10 000 coins.

    (similar to the money cheat, but for a different object)
    """

    load_save_file_for_py()

    obj_list = savefile_as_py['objects']

    for obj in obj_list:
        obj_path = obj['prefabPath']
        obj_data = obj['componentData2']
        obj_index = obj_list.index(obj)

        if obj_path == 'Prefabs/Characters/Banker':
            for obj_data_item in obj_data:
                obj_data_item_index = obj_data.index(obj_data_item)
                if 'stashedCoins' in obj_data_item['data']:
                    (obj_list[obj_index]
                        ['componentData2']
                        [obj_data_item_index]
                        ['data']) = '{"stashedCoins":10000}'

                    print('Banker has 10 000 coins now.')
                    break
            break
    else:
        print('ERROR: Problem while changing Banker data')
        print("Are you sure the Banker is available in your game?")

    close_save_file_for_py()


def switch_mount(mount_choice):
    """
    Looks for the Mount object
    in the savefile and modify its value with
    the mount_choice argument.

    mount_choice can be:
        Normal mounts:
        - 'Horse Regular'
        - 'Warhorse'
        - 'Stag'
        - 'Bear'
        - 'Unicorn'

        Event mounts:
        - 'Reindeer'
        - 'SpookyHorse'

    Make sure the argument passed is a string !

    """
    load_save_file_for_py()

    for obj in obj_list:
        # basic data for each object
        obj_name = obj['name']
        obj_index = obj_list.index(obj)
        obj_path = obj['prefabPath']
        # parent data for each object
        obj_parent = obj['parentObject']
        obj_parent_name = list(obj_parent.values())[0]
        obj_has_parent = len(obj_parent_name) != 0
        # components of each object
        obj_data_list = obj['componentData2']

        # The whole point of next if statement
        # is to figure out which object contains
        # the player's mount data and grab the index
        if 'Prefabs/Steeds' in obj_path \
            and obj_has_parent \
                and 'Player' in obj_parent_name:
            # using the object index,
            # change the value to the present
            # function argument.
            (obj_list[obj_index]
                ['prefabPath']) = 'Prefabs/Steeds/' + mount_choice
            break

    else:
        print('ERROR: Unable to change mount.')

    # Just for consistency,
    # also change the 'preview' data.
    # (the graphics at loading screen).
    player_preview['steedPrefabPath'] = \
        'Prefabs/Steeds/' + mount_choice

    # Informs user which mount has been set.
    print('Mount is now ' + mount_choice.lower())
    # writes modifications to file.
    close_save_file_for_py()


def god_mode(god_state):
    """
    Player becomes invulnerable.
    god_state is a string: 'true' or 'false'
    """

    load_save_file_for_py()

    # look for invulnerable option for Player object
    for obj in obj_list:
        # find the Player object
        obj_path = obj['prefabPath']
        obj_data = obj['componentData2']
        if 'Prefabs/Characters/Player' in obj_path:
            # In the player object,
            # look for the child object named 'componentData2'
            # It contains the most important Player data
            # (coins, color, health...)
            # It is a list of different kinds of data.
            # Some *different* values might have the *same key*
            # But we're looking for the 'data' key paired
            # with the 'invulnerable' value and change this value.
            for player_data in obj_data:
                for key, value in player_data.items():
                    if 'data' in key and 'invulnerable' in value:
                        # save index for value with god option.
                        god_index = obj_data.index(player_data)
                        # now, use index to alter 'componentData2'
                        obj_data[god_index]['data'] = \
                            '{"hitPoints":0,"invulnerable":%s}' % (god_state)
            break
    else:
        print('ERROR: Unable to toggle god mode.')

    close_save_file_for_py()
    # Print a message for user
    if god_state == 'true':
        print('God mode ON')
    elif god_state == 'false':
        print('God mode OFF')


# ARGUMENT HANDLING
parser = argparse.ArgumentParser(
    description='Script to cheat in Kingdom: New Lands.')

parser.add_argument(
    '-w',
    '--wallet',
    action='store_true',
    help="fill player's wallet.")

parser.add_argument(
    '-b',
    '--bank',
    action='store_true',
    help="fill Bank with 10 000 coins")

parser.add_argument(
    '-m',
    '--mount',
    type=str,
    metavar='',
    help='change current mount.')

parser.add_argument(
    '-g',
    '--god',
    type=str,
    metavar='',
    help='enable or disable god mode')

args = parser.parse_args()

# MAIN
if __name__ == '__main__':
    # user can use simple mount names (keys)
    # instead of the ones from the savefile(values).
    mount_choices = {
        'horse': 'Horse Regular',
        'warhorse': 'Warhorse',
        'stag': 'Stag',
        'bear': 'Bear',
        'unicorn': 'Unicorn',
        'reindeer': 'Reindeer',
        'spookyhorse': 'SpookyHorse'}

    # search for the save file
    savefile_dir = find_savefile()
    print('Applying cheat in:  %s' % savefile_dir)
    # run the script only if savefile_dir has been found.
    if savefile_dir:
        os.chdir(savefile_dir)
        cwd_files = os.listdir('.')
        # Creates backup if it doesn't exists
        # Usually, the first time you run this script
        if (BACKUP_FILE not in cwd_files) and (SAVE_FILE in cwd_files):
            shutil.copyfile(SAVE_FILE, BACKUP_FILE)

    else:
        print("ERROR: Save file cannot be found.")
        sys.exit()

    if args.wallet:
        money_hack()
    elif args.bank:
        bank_money()
    elif args.mount:
        if args.mount in mount_choices.keys():
            switch_mount(mount_choices[args.mount])
        else:
            print('ERROR: Invalid mount option.')
            print('')
            print('Use one of these:')
            for key in mount_choices.keys():
                print('  - %s' % (key))
            print('')

    elif args.god:
        if args.god == 'on':
            god_mode('true')
        elif args.god == 'off':
            god_mode('false')
        else:
            print('ERROR: Invalid option for god mode.')
            print('')
            print('Usage is:')
            print('  --god on')
            print('or')
            print('  --god off')
            print('')
    else:
        print('Read help first, use:')
        print('    $ python3 %s --help' % (SCRIPT_NAME))
